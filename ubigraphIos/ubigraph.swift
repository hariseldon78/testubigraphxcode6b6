//
//  ubigraph.swift
//  provaUbigraph1
//
//  Created by Roberto Previdi on 25/08/14.
//  Copyright (c) 2014 roby. All rights reserved.
//

import Foundation

typealias VertexId=Int
typealias EdgeId=Int

var _ubigraph_instance:Ubigraph?
class Ubigraph : NSObject,XMLRPCConnectionDelegate
{
	class func getInstance(url:String="http://localhost:20738/RPC2") -> Ubigraph
	{
		if _ubigraph_instance == nil
		{
			_ubigraph_instance=Ubigraph(url:url)
			
		}
		return _ubigraph_instance!
	}
	
	var url:String
	
	init(url:String)
	{
		self.url=url
		super.init()

	}
	
	func sendRequest(method:String,parameters: [AnyObject!]) ->XMLRPCResponse
	{
		let request=XMLRPCRequest(URL:NSURL(string: self.url))
		request.setMethod(method, withParameters: parameters)
		let err=NSErrorPointer()
		return XMLRPCConnection.sendSynchronousXMLRPCRequest(request, error: err)
	}
	
	func clear()
	{
		sendRequest("ubigraph.clear", parameters: [])
	}
	func setEdgeStyleAttribute(styleId:Int,attribute:String,value:String)
	{
		sendRequest("ubigraph.set_edge_style_attribute", parameters: [styleId,attribute,value])
	}
	func setVertexStyleAttribute(styleId:Int,attribute:String,value:String)
	{
		sendRequest("ubigraph.set_vertex_style_attribute", parameters: [styleId,attribute,value])
	}
	func newVertex()->VertexId
	{
		return (sendRequest("ubigraph.new_vertex", parameters: []).object() as NSNumber ).integerValue
	}
	func newEdge(#v1:VertexId,v2:VertexId)->EdgeId
	{
		return (sendRequest("ubigraph.new_edge", parameters: [v1 as NSNumber,v2 as NSNumber]).object() as NSNumber ).integerValue
	}
	
	
	func request(request: XMLRPCRequest!, didReceiveResponse response: XMLRPCResponse!) {
		println(request.method())
		println(request.parameters())
		println(response.body())
	}
	func request(request: XMLRPCRequest!, didFailWithError error: NSError!) {
		println(error.description)
	}
	func request(request: XMLRPCRequest!, canAuthenticateAgainstProtectionSpace protectionSpace: NSURLProtectionSpace!) -> Bool {
		println("canAuthenticate")
		return true
	}
	func request(request: XMLRPCRequest!, didReceiveAuthenticationChallenge challenge: NSURLAuthenticationChallenge!) {
		println("didReceiveAuthenticationChallenge")
	}
	func request(request: XMLRPCRequest!, didCancelAuthenticationChallenge challenge: NSURLAuthenticationChallenge!) {
		println("didCancelAuthenticationChallenge")
	}
}